# Courier Kata

By Mikiel Agutu

The CLI project will print a sample output to the command line. The code can be changed to see the prices of different sets of parcels.

The business logic lives in the `ParcelCostEngine` project, mainly in `ParcelDeliveryQuoteCalculator.cs`.

The unit tests in `ParcelCostEngine.Tests` can be run also.

# Future changes

- Add more tests to cover the edge cases for multi discount
    - Requirement 'The combination of discounts which saves the most money should be selected every time', I'm not 100% confident this will happen right now
    - e.g. 8 small parcels + 3 medium parcels + an XL parcel
    - Meets spec for 2 small discounts, 1 medium, and 2 multi discounts
    - Not sure if all should apply
    - Requirement implies some kind of multi exclusivity - e.g you could pick either small, medium, or multi, but only apply the biggest saving?
    - I would push back/clarify this requirement - does it make sense? Surely one 'type' of discount should apply, rather than all of them
    - e.g. small discount only applies if all are small
    - Is it even easy for the user to understand? Will it impact behavior e.g encourage placing bigger orders if the deals are too complicated?
- Clarify rules around discounts
    - e.g, does mixed parcel discount stack with medium and small?
- Clarify possible contridiction / naive requirement for discounts:
    - Requirement says 'cheapest parcel is the free one'
    - However, the example seems to imply the order in which parcels are inputted matters:
    - i.e. it's doing 8, 8, 8 | 10, 10, 10 - so discount 8 and then 10
    - This is possibly a naive example, or phrasing of the requirement
    - Does it make sense to get a bigger discount based on the order parcels are inputted? 
    - e.g: 8, 8, 10 | 8, 8, 10 = 16 discount
    - e.g: 8, 8, 8 | 10, 10, 10 = 18 discount
    - My approach will give the two cheapest as discounts, regardless of order inputted - I would clarify if this meets business requirements
- Extract a class for the discount engine
    - I'd refactor this so the logic is easier to follow
    - It's likely this will change frequently, so it needs to be easy to amend
    - Perhaps a distant goal is to create some kind of DSL so users can define their own discount rules without rebuilding the application
- A better breakdown of how the pricing is calculated
    - Show the user the extra cost for weight for instance
    - To make the application more usable, show users how they can save money
- Return a wrapper object for the delivery quotes rather than a collection of individual quotes
    - Easier to deal with
    - Can add helper methods e.g get overall cost