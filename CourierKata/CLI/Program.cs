﻿using System;
using System.Linq;
using ParcelCostEngine;

namespace CLI
{
    class Program
    {
        static void Main(string[] args)
        {
            var calculator = new ParcelDeliveryQuoteCalculator();

            var result = calculator.Calculate(new[]
            {
                new Parcel(5, 5, 5, 1),
                new Parcel(25, 25, 25, 2),
                new Parcel(55, 55, 55, 5),
                new Parcel(105, 105, 105, 2),
                new Parcel(105, 105, 105, 55)
            });

            foreach (var parcelDeliveryQuote in result)
            {
                Console.WriteLine("Quote for " + parcelDeliveryQuote.Type + " parcel:");

                if (parcelDeliveryQuote.Discounted)
                {
                    Console.WriteLine("Discount!");
                    Console.WriteLine("You saved " + parcelDeliveryQuote.NormalShipping + " on normal shipping");
                    Console.WriteLine("You saved " + parcelDeliveryQuote.SpeedyShipping + " on speedy shipping");
                }
                else
                {
                    Console.WriteLine("Normal shipping: " + parcelDeliveryQuote.NormalShipping);
                    Console.WriteLine("Speedy shipping: " + parcelDeliveryQuote.SpeedyShipping);
                }

                Console.WriteLine();
            }
            
            var normalShippingSum = result.Where(x => !x.Discounted).Sum(x => x.NormalShipping.MinorUnits);
            var speedyShippingSum = result.Where(x => !x.Discounted).Sum(x => x.SpeedyShipping.MinorUnits);
            Console.WriteLine("Total cost normal shipping: " + new Currency(normalShippingSum));
            Console.WriteLine("Total cost speedy shipping: " + new Currency(speedyShippingSum));
        }
    }
}
