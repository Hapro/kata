﻿using System.Collections.Generic;
using System.Linq;
using NUnit.Framework;
using Shouldly;

namespace ParcelCostEngine.Tests
{
    public class ParcelDeliveryQuoteCalculatorShould
    {
        private ParcelDeliveryQuoteCalculator _parcelDeliveryQuoteCalculator;

        [SetUp]
        public void SetUp()
        {
            _parcelDeliveryQuoteCalculator = new ParcelDeliveryQuoteCalculator();
        }

        [TestCaseSource(nameof(Calculate_TestCaseData))]
        public void Calculate(
            Parcel parcel,
            int expectedNormalShippingMinorUnits,
            int expectedSpeedyShippingMinorUnits)
        {
            var result = _parcelDeliveryQuoteCalculator.Calculate(parcel);
            result.NormalShipping.MinorUnits.ShouldBe(expectedNormalShippingMinorUnits);
            result.SpeedyShipping.MinorUnits.ShouldBe(expectedSpeedyShippingMinorUnits);
        }

        [TestCaseSource(nameof(Calculate_WhenOverWeightLimit_TestCaseData))]
        public void Calculate_WhenOverWeightLimit(
            Parcel parcel,
            int expectedNormalShippingMinorUnits,
            int expectedSpeedyShippingMinorUnits)
        {
            var result = _parcelDeliveryQuoteCalculator.Calculate(parcel);
            result.NormalShipping.MinorUnits.ShouldBe(expectedNormalShippingMinorUnits);
            result.SpeedyShipping.MinorUnits.ShouldBe(expectedSpeedyShippingMinorUnits);
        }

        [TestCaseSource(nameof(Calculate_WithSpecialDiscounts_TestCaseData))]
        public void Calculate_WithSpecialDiscounts(IEnumerable<Parcel> parcels,
            int expectedNormalShippingMinorUnits,
            int expectedSpeedyShippingMinorUnits)
        {
            var result = _parcelDeliveryQuoteCalculator.Calculate(parcels.ToArray());
            result.Where(x => !x.Discounted).Sum(x => x.NormalShipping.MinorUnits).ShouldBe(expectedNormalShippingMinorUnits);
            result.Where(x => !x.Discounted).Sum(x => x.SpeedyShipping.MinorUnits).ShouldBe(expectedSpeedyShippingMinorUnits);
        }

        private static IEnumerable<TestCaseData> Calculate_WithSpecialDiscounts_TestCaseData()
        {
            // Four small parcels
            // One should be free
            var parcelBatchOne = new List<Parcel>
            {
                new(5, 5, 5, 1), // 300
                new (5, 5, 5, 1), // 300
                new (5, 5, 5, 1), // 300
                new (5, 5, 5, 1) // 300
            };

            var testCaseOne = new TestCaseData(parcelBatchOne, 900, 1800);

            // Four small parcels
            // Two are overweight, so cost more
            var parcelBatchTwo = new List<Parcel>
            {
                new (5, 5, 5, 3), // 700
                new(5, 5, 5, 1), // 300 <- discounted
                new (5, 5, 5, 2), // 500
                new (5, 5, 5, 1) // 300
            };

            var testCaseTwo = new TestCaseData(parcelBatchTwo, 1500, 3000);

            // Three medium parcels
            // One should be free
            var parcelBatchThree = new List<Parcel>
            {
                new (25, 25, 25, 1), // 800
                new (25, 25, 25, 1), // 800
                new (25, 25, 25, 1), // 800
            };

            var testCaseThree = new TestCaseData(parcelBatchThree, 1600, 3200);

            // Three medium parcels
            // Two are overweight, so cost more
            var parcelBatchFour = new List<Parcel>
            {
                new (25, 25, 25, 1), // 800 <- discounted
                new (25, 25, 25, 4), // 1000
                new (25, 25, 25, 4), // 1000
            };
            var testCaseFour = new TestCaseData(parcelBatchFour, 2000, 4000);


            // Six medium parcels
            // Two are overweight, so cost more
            var parcelBatchFive = new List<Parcel>
            {
                new (25, 25, 25, 1), // 800 <- discounted
                new (25, 25, 25, 4), // 1000
                new (25, 25, 25, 4), // 1000
                new (25, 25, 25, 1), // 800 <- discounted
                new (25, 25, 25, 4), // 1000
                new (25, 25, 25, 4), // 1000
            };
            var testCaseFive = new TestCaseData(parcelBatchFive, 4000, 8000);

            // Eight small parcels
            // Two are overweight, so cost more
            var parcelBatchSix = new List<Parcel>
            {
                new(5, 5, 5, 1), // 300 <- discounted
                new(5, 5, 5, 1), // 300 <- discounted
                new (5, 5, 5, 2), // 500
                new (5, 5, 5, 2), // 500
                new (5, 5, 5, 2), // 500
                new (5, 5, 5, 2), // 500
                new (5, 5, 5, 2), // 500
                new (5, 5, 5, 2), // 500

            };
            var testCaseSix = new TestCaseData(parcelBatchSix, 3000, 6000);

            // Mixed parcels
            var parcelBatchSeven = new List<Parcel>
            {
                new (200, 200, 200, 1), // XL parcel - 2500
                new (5, 5, 5, 4), // Small parcel - 900
                new (5, 5, 5, 3), // Small parcel - 700
                new (5, 5, 5, 2), // Small parcel - 500 <- discounted (5th parcel)
                new (5, 5, 5, 1), // Small parcel - 300  <- discounted (4th small parcel)
                new (25, 25, 25, 5) // Medium parcel - 1200 

            };
            var testCaseSeven = new TestCaseData(parcelBatchSeven, 5300, 10600);

            yield return testCaseOne;
            yield return testCaseTwo;
            yield return testCaseThree;
            yield return testCaseFour;
            yield return testCaseFive;
            yield return testCaseSix;
            yield return testCaseSeven;
        }

        private static IEnumerable<TestCaseData> Calculate_WhenOverWeightLimit_TestCaseData()
        {
            // Small parcels
            yield return new TestCaseData(new Parcel(5, 5, 5, 0), 300, 600);
            yield return new TestCaseData(new Parcel(5, 5, 5, 1), 300, 600);
            yield return new TestCaseData(new Parcel(5, 5, 5, 2), 500, 1000); // 1kg over limit
            yield return new TestCaseData(new Parcel(5, 5, 5, 3), 700, 1400); // 2kg over limit
            yield return new TestCaseData(new Parcel(5, 5, 5, 4), 900, 1800); // 3kg over limit

            // Medium parcels
            yield return new TestCaseData(new Parcel(25, 25, 25, 0), 800, 1600);
            yield return new TestCaseData(new Parcel(25, 25, 25, 3), 800, 1600);
            yield return new TestCaseData(new Parcel(25, 25, 25, 4), 1000, 2000); // 1kg over limit
            yield return new TestCaseData(new Parcel(25, 25, 25, 5), 1200, 2400); // 2kg over limit
            yield return new TestCaseData(new Parcel(25, 25, 25, 6), 1400, 2800); // 3kg over limit

            // Large parcels
            yield return new TestCaseData(new Parcel(80, 80, 80, 0), 1500, 3000);
            yield return new TestCaseData(new Parcel(80, 80, 80, 6), 1500, 3000);
            yield return new TestCaseData(new Parcel(80, 80, 80, 7), 1700, 3400); // 1kg over limit
            yield return new TestCaseData(new Parcel(80, 80, 80, 8), 1900, 3800); // 2kg over limit
            yield return new TestCaseData(new Parcel(80, 80, 80, 9), 2100, 4200); // 3kg over limit

            // XL parcels
            yield return new TestCaseData(new Parcel(200, 200, 200, 0), 2500, 5000);
            yield return new TestCaseData(new Parcel(200, 200, 200, 10), 2500, 5000);
            yield return new TestCaseData(new Parcel(200, 200, 200, 11), 2700, 5400); // 1kg over limit
            yield return new TestCaseData(new Parcel(200, 200, 200, 12), 2900, 5800); // 2kg over limit
            yield return new TestCaseData(new Parcel(200, 200, 200, 13), 3100, 6200); // 3kg over limit

            // Heavy parcels
            yield return new TestCaseData(new Parcel(100, 100, 100, 50), 5000, 10000); // Borderline for heavy parcel
            yield return new TestCaseData(new Parcel(100, 100, 100, 51), 5100, 10200); // 1kg over limit
            yield return new TestCaseData(new Parcel(100, 100, 100, 52), 5200, 10400); // 2kg over limit
            yield return new TestCaseData(new Parcel(100, 100, 100, 53), 5300, 10600); // 3kg over limit
        }

        private static IEnumerable<TestCaseData> Calculate_TestCaseData()
        {
            var smallParcels = new List<Parcel>
            {
                // Edge cases
                new(0, 0, 0, 1),
                new (9, 0, 0, 1),
                new (0, 9, 0, 1),
                new (0, 0, 9, 1),

                // Realistic cases
                new (8, 3, 2, 1),
                new (9, 7, 6, 1)
            };

            var mediumParcels = new List<Parcel>
            {
                // Edge cases
                new (49, 0, 0, 1),
                new (0, 49, 0, 1),
                new (0, 0, 49, 1),

                // Realistic cases
                new (12, 30, 25, 1),
                new (30, 19, 10, 1)
            };

            var largeParcels = new List<Parcel>
            {
                // Edge cases
                new (99, 0, 0, 1),
                new (0, 99, 0, 1),
                new (0, 0, 99, 1),

                // Realistic cases
                new (80, 30, 25, 1),
                new (60, 84, 93, 1)
            };

            var xlParcels = new List<Parcel>
            {
                // Edge cases
                new (100, 0, 0, 1),
                new (0, 100, 0, 1),
                new (0, 0, 100, 1),

                // Realistic cases
                new (1000, 90, 25, 1),
                new (234, 22, 200, 1)
            };

            var smallParcelTestCases = smallParcels
                .Select(x => new TestCaseData(x, 300, 600));

            var mediumParcelTestCases = mediumParcels
                .Select(x => new TestCaseData(x, 800, 1600));

            var largeParcelTestCases = largeParcels
                .Select(x => new TestCaseData(x, 1500, 3000));

            var xlParcelTestCases = xlParcels
                .Select(x => new TestCaseData(x, 2500, 5000));

            return smallParcelTestCases
                .Concat(mediumParcelTestCases)
                .Concat(largeParcelTestCases)
                .Concat(xlParcelTestCases);
        }
    }
}
