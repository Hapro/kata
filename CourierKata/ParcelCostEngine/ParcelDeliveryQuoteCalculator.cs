﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ParcelCostEngine
{
    public class ParcelDeliveryQuoteCalculator
    {
        private const int SmallParcelCostMinorUnits = 300;
        private const int MediumParcelCostMinorUnits = 800;
        private const int LargeParcelCostMinorUnits = 1500;
        private const int XlParcelCostMinorUnits = 2500;
        private const int HeavyParcelCostMinorUnits = 5000;

        public List<ParcelDeliveryQuote> Calculate(params Parcel[] parcels)
        {
            return CalculateParcelDeliveryQuotesWithDiscounts(parcels);
        }

        private List<ParcelDeliveryQuote> CalculateParcelDeliveryQuotesWithDiscounts(IEnumerable<Parcel> parcels)
        {
            var parcelQuotes = parcels
                .Select(parcel => Tuple.Create(parcel, Calculate(parcel)))
                .ToList();

            var smallParcels = parcelQuotes
                .Where(x => IsSmallParcel(x.Item1))
                .ToList();

            var mediumParcels = parcelQuotes
                .Where(x => IsMediumParcel(x.Item1) && !IsSmallParcel(x.Item1))
                .ToList();

            var numberOfSmallParcelDealsAvailable = smallParcels.Count / 4;
            var numberOfMediumParcelDealsAvailable = mediumParcels.Count / 3;
            var numberOfMultiParcelDealsAvailable = parcelQuotes.Count / 5;

            // If it's all small or all medium parcels, then the multi deal doesn't apply
            if (smallParcels.Count == parcelQuotes.Count || mediumParcels.Count == parcelQuotes.Count)
            {
                numberOfMultiParcelDealsAvailable = 0;
            } 

            var discountedSmallParcels = smallParcels
                .OrderBy(x => x.Item2.NormalShipping.MinorUnits)
                .Take(numberOfSmallParcelDealsAvailable)
                .ToList();

            var discountedMediumParcels = mediumParcels
                .OrderBy(x => x.Item2.NormalShipping.MinorUnits)
                .Take(numberOfMediumParcelDealsAvailable)
                .ToList();

            var discountedMultiParcels = parcelQuotes
                .Where(x => discountedSmallParcels.All(y => x.Item1 != y.Item1))
                .Where(x => discountedMediumParcels.All(y => x.Item1 != y.Item1))
                .OrderBy(x => x.Item2.NormalShipping.MinorUnits)
                .Take(numberOfMultiParcelDealsAvailable)
                .ToList();

            var result = parcelQuotes
                .Where(x => discountedSmallParcels.All(y => x.Item1 != y.Item1))
                .Where(x => discountedMediumParcels.All(y => x.Item1 != y.Item1))
                .Where(x => discountedMultiParcels.All(y => x.Item1 != y.Item1))
                .Select(x => x.Item2)
                .Concat(discountedSmallParcels.Select(x => CreateDiscountParcelDeliveryQuote(x.Item2)))
                .Concat(discountedMediumParcels.Select(x => CreateDiscountParcelDeliveryQuote(x.Item2)))
                .Concat(discountedMultiParcels.Select(x => CreateDiscountParcelDeliveryQuote(x.Item2)))
                .ToList();

            return result;
        }

        private static ParcelDeliveryQuote CreateDiscountParcelDeliveryQuote(ParcelDeliveryQuote parcelDeliveryQuote)
        {
            return new(parcelDeliveryQuote.NormalShipping,
                parcelDeliveryQuote.SpeedyShipping,
                parcelDeliveryQuote.Parcel, 
                true,
                parcelDeliveryQuote.Type);
        }

        public ParcelDeliveryQuote Calculate(Parcel parcel)
        {
            if (parcel.WeightKg >= 50)
            {
                return CreateHeavyParcelDeliveryQuote(parcel);
            }

            if (IsSmallParcel(parcel))
            {
                return CreateStandardParcelDeliveryQuote(parcel, SmallParcelCostMinorUnits, 1, "Small");
            }

            if (IsMediumParcel(parcel))
            {
                return CreateStandardParcelDeliveryQuote(parcel, MediumParcelCostMinorUnits, 3, "Medium");
            }

            if (IsLargeParcel(parcel))
            {
                return CreateStandardParcelDeliveryQuote(parcel, LargeParcelCostMinorUnits, 6, "Large");
            }

            return CreateStandardParcelDeliveryQuote(parcel, XlParcelCostMinorUnits, 10, "XL");
        }

        private static ParcelDeliveryQuote CreateHeavyParcelDeliveryQuote(Parcel parcel)
        {
            return CreateParcelDeliveryQuote(parcel, HeavyParcelCostMinorUnits, 50, 100, "Heavy");
        }

        private static ParcelDeliveryQuote CreateStandardParcelDeliveryQuote(Parcel parcel, int baseCostMinorUnits, int weightLimitKg, string type)
        {
            return CreateParcelDeliveryQuote(parcel, baseCostMinorUnits, weightLimitKg, 200, type);
        }

        private static ParcelDeliveryQuote CreateParcelDeliveryQuote(
            Parcel parcel,
            int baseCostMinorUnits,
            int weightLimitKg,
            int surchargePerKgOverweightMinorUnits,
            string type)
        {
            var kgsOverWeightLimit = parcel.WeightKg - weightLimitKg;
            var weightSurchargeMinorUnits = 0;

            if (kgsOverWeightLimit > 0)
            {
                weightSurchargeMinorUnits = kgsOverWeightLimit * surchargePerKgOverweightMinorUnits;
            }

            var overallBaseCostMinorUnits = baseCostMinorUnits + weightSurchargeMinorUnits;

            return new ParcelDeliveryQuote(
                new Currency(overallBaseCostMinorUnits), 
                new Currency(overallBaseCostMinorUnits * 2),
                parcel,
                false,
                type);
        }

        private static bool IsSmallParcel(Parcel parcel) => 
            AreAllParcelDimensionsLessThan(parcel, 10);

        private static bool IsMediumParcel(Parcel parcel) =>
            AreAllParcelDimensionsLessThan(parcel, 50);

        private static bool IsLargeParcel(Parcel parcel) =>
            AreAllParcelDimensionsLessThan(parcel, 100);

        private static bool AreAllParcelDimensionsLessThan(Parcel parcel, int magnitudeInCm)
        {
            return parcel.WidthCm < magnitudeInCm &&
                   parcel.HeightCm < magnitudeInCm &&
                   parcel.DepthCm < magnitudeInCm;
        }
    }
}
