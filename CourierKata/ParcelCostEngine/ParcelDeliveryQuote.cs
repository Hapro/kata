﻿namespace ParcelCostEngine
{
    public class ParcelDeliveryQuote
    {
        public ParcelDeliveryQuote(Currency normalShipping, Currency speedyShipping, Parcel parcel, bool discounted, string type)
        {
            NormalShipping = normalShipping;
            SpeedyShipping = speedyShipping;
            Parcel = parcel;
            Discounted = discounted;
            Type = type;
        }

        public Currency NormalShipping { get; }
        public Currency SpeedyShipping { get; }
        public Parcel Parcel { get; }
        public bool Discounted { get; }
        public string Type { get; }
    }
}