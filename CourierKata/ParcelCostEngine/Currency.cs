﻿namespace ParcelCostEngine
{
    public class Currency
    {
        public Currency(int minorUnits)
        {
            MinorUnits = minorUnits;
        }

        public int MinorUnits { get; }

        public override string ToString()
        {
            return "$" + (double)MinorUnits / 100;
        }
    }
}
