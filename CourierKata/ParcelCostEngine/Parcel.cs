﻿namespace ParcelCostEngine
{
    public class Parcel
    {
        public Parcel(int widthCm, int heightCm, int depthCm, int weightKg)
        {
            WidthCm = widthCm;
            HeightCm = heightCm;
            DepthCm = depthCm;
            WeightKg = weightKg;
        }

        public int WidthCm { get; }

        public int HeightCm { get; }

        public int DepthCm { get; }
        public int WeightKg { get; }
    }
}
